const express = require('express')
const morgan = require('morgan')
const path = require('path')
const engine = require('ejs-mate')
const app = express()

app.engine('ejs', engine)
app.set('views', path.join(__dirname, '/views'))
app.set('view engine', 'ejs')

app.use(express.static('public'))
app.use(express.json())
app.use(morgan('dev'))

app.use('/api', require('./rutas/api'))
app.use('/', require('./rutas/main'))

app.use('/', (req, res, next) => {
  res.status(404).render('404')
})

app.listen(8000, () => {
  console.log('run')
})
